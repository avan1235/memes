import { MemesList } from "./src/memes_list";
import { Meme } from "./src/memes";
import * as DB from './src/db';
import { echo, exit } from "shelljs";

const LOCAL_MEME_PREFIX = '/static/img/';
const MIN_PRICE = 100;
const INIT_PRICES_COUNT = 20;
const MAX_PRICE = 10000;

const memesData = new MemesList(generateMemesData());

const resetDbConnection = DB.getConnectionForRW();

resetAllData()
    .then(generateBaseUsers, fatal)
    .then(insertGeneratedData, fatal)
    .then(printGeneratedData, fatal);

function generateMemesData(): Meme[] {
    const memesPlainData = [
        ['biblio.jpg', 'Wystawka w bibliotece'],
        ['cp.png', 'Programowanie Współbieżne'],
        ['first_year.jpg', 'Niespodzianka pierwszoroczna'],
        ['jao.png', 'Nic Nowego w Nowym Roku'],
        ['short_way.jpg', 'Routine'],
        ['where.png', 'Każdy się dziwi']
    ].map((values: string[], index: number) => {
        const path = `${LOCAL_MEME_PREFIX}${values[0]}`;
        const meme = new Meme(index, values[1], generateRandomPrices(), path);
        return meme;
    });
    return memesPlainData;
}

function randomPrice(): number {
    return parseInt(`${(Math.random() * (MAX_PRICE - MIN_PRICE) + MIN_PRICE)}`, 10);
}

function generateRandomPrices(): number[] {
    const result: number[] = [];
    for (let index = 0; index < INIT_PRICES_COUNT; index++) {
        result.push(randomPrice());
    }
    return result;
}

function resetAllData(): Promise<void> {
    return DB.reset(resetDbConnection).then(() => {
        echo('Database successfully reset');
    }, err => {
        fatal(err, 'Database reset error');
    });
}

function insertGeneratedData(): Promise<void[]> {
    const result: Promise<void>[] = [];
    memesData.allCollected.forEach(meme => {
        const pojo = meme.toPojo();
        const single = DB.addMeme(pojo, resetDbConnection);
        pojo.prices.map(price => {
            result.push(DB.addMemePrice(price, pojo.id, 'admin', resetDbConnection));
        });
        result.push(single);
    });
    return Promise.all(result);
}

function generateBaseUsers(): Promise<void[]> {
    const sampleUsers = [['admin', 'admin'], ['user', 'user']];
    const result: Promise<void>[] = [];
    sampleUsers.forEach(userData => {
        const [username, password] = userData;
        const single = DB.addUser(username, password, resetDbConnection);
        result.push(single);
    });
    return Promise.all(result);
}

function printGeneratedData(): Promise<void> {
    return DB.getAllMemes(resetDbConnection).then(latest => {
        echo('Data from database:');
        latest.forEach(meme => {
            echoMemeData(meme);
        });
    }, err => {
        fatal(err, 'Getting data error');
    });
}

function fatal(err: Error, description: string = ""): void {
    echo(`Got error: ${err}`);
    echo(description);
    exit(1);
}

function echoMemeData(meme: DB.MemeCurrentDao) {
    echo(JSON.stringify(meme));
}