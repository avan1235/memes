import express from 'express';
import dotenv from 'dotenv';
import { join } from 'path';
import { Meme } from './memes';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import csurf from 'csurf';
import * as DB from './db';
import { echo } from 'shelljs';
import { Database } from 'sqlite3';

const DEFAULT_PORT = 8080;
const DEFAULT_SECRET_KEY = 'University of Warsaw';
const DEFAULT_USE_HTTPS = false;

const MAX_DB_SAVE_TRIES = 5;
const MAX_WAIT_NEXT_TRY_MILLIS = 100;

const app = express();
dotenv.config();

const csrfProtect = csurf({ cookie: true });
const SECRET_KEY: string = process.env.SECRET_KEY || DEFAULT_SECRET_KEY;
const USE_HTTPS = process.env.USE_HTTPS !== undefined ? process.env.USE_HTTPS === 'true' : DEFAULT_USE_HTTPS;

// Express configuration
app.set('view engine', 'pug');
app.set('port', process.env.PORT || DEFAULT_PORT);

if (USE_HTTPS) {
    app.set('trust proxy', 1);
}

app.use(sslRedirect());
app.use('/static', express.static(join(__dirname, './public')));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cookieParser());
app.use(session({
    secret: SECRET_KEY,
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure: USE_HTTPS
    }
}));

// Primary app routes.
app.get('/', (req, res) => {
    const connection = DB.getConnectionForRO();
    DB.getMostValueableMemes(connection).then(memesData => {
        connection.close();
        res.render('index', {
            title: 'MIM - Memes Interactive Market',
            message: 'Witaj na',
            title_body: 'Najdroższe memy',
            memes: memesData,
            refName: 'Pokaż wszystkie',
            ref: '/all',
            showRef: true,
            isLogged: isLoggedUser(req)
        });
    }, err => {
        handleError(err, '/', res, connection);
    }).catch(err => {
        handleError(err, '/', res, connection);
    });
});

app.get('/all', (req, res) => {
    const connection = DB.getConnectionForRO();
    DB.getAllMemes(connection).then(memesData => {
        connection.close();
        res.render('index', {
            title: 'MIM - Memes Interactive Market',
            message: 'Witaj na',
            title_body: 'Wszystkie memy',
            memes: memesData,
            showRef: true,
            refName: 'Powrót',
            ref: '/',
            isLogged: isLoggedUser(req)
        });
    }, err => {
        handleError(err, '/all', res, connection);
    }).catch(err => {
        handleError(err, '/all', res, connection);
    });
});

app.post('/meme/:memeId(\\d+)', csrfProtect, (req, res) => {
    if (!isLoggedUser(req)) {
        res.redirect('/error');
    }
    const connection = DB.getConnectionForRW();
    try {
        const user = req.session.user;
        const memeId = parseInt(req.params.memeId, 10); // regex validated
        const newPrice = req.body.price;
        const parsedPrice = parseInt(newPrice, 10);
        tryAddMeme(res, parsedPrice, memeId, user, connection);
    } catch(err) {
        handleError(err, '/meme/:memeId [POST]', res, connection);
    }
});

app.get('/meme/:memeId(\\d+)', csrfProtect, (req, res) => {
    const connection = DB.getConnectionForRO();
    const memeId = parseInt(req.params.memeId, 10); // regex validated
    DB.getMemeById(memeId, connection).then(meme => {
        connection.close();
        res.setHeader('CSRF-Header', req.csrfToken());
        res.json({id: meme.id, name: meme.name, url: meme.url, prices: meme.prices});
    }, err => {
        handleError(err, '/meme/:memeId [GET]', res, connection);
    }).catch(err => {
        handleError(err, '/meme/:memeId [POST]', res, connection);
    });
});

app.get('/meme', csrfProtect, (req, res) => {
    const connection = DB.getConnectionForRO();
    try {
        const paramId = req.query.id;
        const memeId = parseInt(`${paramId}`, 10);
        DB.getMemeById(memeId, connection).then(meme => {
            connection.close();
            res.setHeader('CSRF-Header', req.csrfToken());
            renderMeme(res, req, new Meme(meme.id, meme.name, meme.prices, meme.url));
        }, err => {
            handleError(err, '/meme?=memeId [GET]', res, connection);
        }).catch(err => {
            handleError(err, '/meme?=memeId [GET]', res, connection);
        });
    } catch(err) {
        handleError(err, '/meme?=memeId [GET]', res, connection);
    }
});

app.get('/login', (req, res) => {
    if (isLoggedUser(req)) {
        res.redirect('/');
        return;
    }
    res.render('login', {
        title: 'Login to MIM - Memes Interactive Market',
        refName: 'Strona główna',
        ref: '/',
        showRef: true,
        isLogged: isLoggedUser(req)
    });
});

app.get('/logout', (req, res) => {
    req.session.destroy(err => {
        if (err) {
            handleError(err, '/logout', res);
        }
        res.redirect('/');
    });
});

app.post('/auth', (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    const connection = DB.getConnectionForRO();
    DB.validateUser(username, password, connection).then(isValid => {
        if (isValid) {
            req.session.regenerate(err => {
                if (err) {
                    handleError(err, '/auth', res, connection);
                    return;
                }
                req.session.user = username;
                res.redirect('/');
            });
        }
        else {
            res.redirect('/error');
        }
        connection.close();
    }, err => {
        handleError(err, '/auth', res, connection);
    }).catch(err => {
        handleError(err, '/auth', res, connection);
    });
});

app.get('/error', (req, res) => {
    res.status(404);
    res.render('404', {
        title: 'Błąd',
        showRef: true,
        refName: 'Strona główna',
        ref: '/',
        isLogged: isLoggedUser(req)
    });
});

app.all('*', (_req, res) => {
    res.redirect('/error');
});


// Helper functions

function wait(timeMillis: number): Promise<void> {
    return new Promise((resolve, _) => {
        window.setTimeout(resolve, timeMillis)
    });
}


function renderMeme(res: express.Response<any>, req: express.Request<any>, renderedMeme: Meme): void {
    res.render('meme', {
        title: renderedMeme.name + ' at MIM - Memes Interactive Market',
        meme: renderedMeme,
        refName: 'Top memy',
        ref: '/',
        showRef: true,
        isLogged: isLoggedUser(req)
    });
}

function handleError(err: Error, path: string, res: express.Response<any>, connectionToClose?: Database): void {
    echo(`Error on ${path}:`);
    echo(`${err}`);
    if (connectionToClose !== undefined) {
        connectionToClose.close();
    }
    res.redirect('/error');
}

function isLoggedUser(req: express.Request<any>): boolean {
    return req.session.user;
}

function tryAddMeme(res: express.Response<any>, parsedPrice: number, memeId: number, username: string, connection: Database, leftTries: number = MAX_DB_SAVE_TRIES): void {
    DB.addMemePrice(parsedPrice, memeId, username, connection).then(() => {
        connection.close();
        res.end();
    }, err => {
        tryAddMemeHandler(res, parsedPrice, memeId, username, leftTries, err, connection);
    }).catch(err => {
        tryAddMemeHandler(res, parsedPrice, memeId, username, leftTries, err, connection);
    });
}

function tryAddMemeHandler(res: express.Response<any>, parsedPrice: number, memeId: number, username: string, leftTries: number, err: Error, connection: Database): void {
    if (leftTries === 0) {
        handleError(err, '/meme/:memeId [POST]', res, connection);
    }
    else {
        const waitMillis = MAX_WAIT_NEXT_TRY_MILLIS * Math.random();
        wait(waitMillis).then(() => {
            tryAddMeme(res, parsedPrice, memeId, username, connection, leftTries - 1);
        });
    }
}


function sslRedirect(): express.RequestHandler  {
    return (req: express.Request<any>, res: express.Response<any>, next: express.NextFunction) => {
        if (USE_HTTPS) {
            if (req.headers['x-forwarded-proto'] !== 'https') {
                res.redirect(302, 'https://' + req.hostname + req.originalUrl);
            }
            else {
                next();
            }
        }
        else {
            next();
        }
    };
}

export default app;