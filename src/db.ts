import * as db from 'sqlite3';
import { MemePojo } from './memes';
import { exit, echo } from 'shelljs';
import * as crypt from 'bcrypt';

export const NAME = 'memes-database.sqlite';
const SALT_ROUNDS = 10;

export { Database } from 'sqlite3';

export function getConnectionForRW(): db.Database {
    return new db.Database(NAME, (err: Error) => {
        if (err) {
            echo('Connection to RW not estabilished, Exiting...');
            exit(1);
        }
        echo('Connection to RW database opened');
    });
};

export function getConnectionForRO(): db.Database {
    return new db.Database(NAME, db.OPEN_READONLY, (err: Error) => {
        if (err) {
            echo('Connection to RO not estabilished, Exiting...');
            exit(1);
        }
        echo('Connection to RO database opened');
    });
};

export interface MemeCurrentDao {
    id: number;
    name: string;
    url: string;
    currentPrice: number;
}

export interface MemeWithHistoryPriceDao {
    id: number;
    name: string;
    url: string;
    prices: number[];
}

interface User {
    username: string;
    password_hash: string;
}

export function reset(dbForRW: db.Database): Promise<void> {
    return new Promise((resolve, reject) => {
        dbForRW.exec(`
        drop table if exists memes_price;
        drop table if exists meme;
        drop table if exists user;
        create table user (
            username text primary key,
            password_hash text not null
        );
        create table meme (
            id integer primary key,
            name text not null,
            url text not null
        );
        create table memes_price (
            id integer primary key,
            price integer not null,
            meme_id integer not null,
            set_by text not null,
            foreign key(meme_id) references meme(id),
            foreign key(set_by) references user(username),
            constraint price_nonnegative check (price >= 0)
        );`, (err: Error) => {
            if (err) {
                reject(err);
                return;
            }
            resolve();
        });
    });
}

export function addUser(username: string, password: string, dbForRW: db.Database): Promise<void> {
    return new Promise((resolve, reject) => {
        crypt.hash(password, SALT_ROUNDS, (err, encrypted) => {
            if (err) {
                reject(err);
                return;
            }
            dbForRW.run(`
            insert into user (username, password_hash)
            values (?, ?);
            `, [username, encrypted], (errDb: Error) => {
                if (errDb) {
                    reject(errDb);
                    return;
                }
                resolve();
            });
        });
    });
}

export function validateUser(username: string, password: string, dbForRO: db.Database): Promise<boolean> {
    return new Promise((resolve, reject) => {
        dbForRO.get(`
        select username, password_hash from user
        where username = ?;
        `, [username], (err: Error, hashed: User) => {
            if (err) {
                reject(err);
                return;
            }
            if (hashed === undefined) {
                reject(Error('No user with specified username'));
                return;
            }
            crypt.compare(password, hashed.password_hash,
                (errCrypt: Error, same: boolean) => {
                    if (errCrypt) {
                        reject(errCrypt);
                        return;
                    }
                    resolve(same);
            });
        });
    });
}

export function addMeme(meme: MemePojo, dbForRW: db.Database): Promise<void> {
    return new Promise((resolve, reject) => {
        dbForRW.run(`
        insert into meme (id, name, url)
        values (?, ?, ?);
        `, [meme.id, meme.name, meme.url], (err: Error) => {
            if (err) {
                reject(err);
                return;
            }
            resolve();
        });
    });
}

export function addMemePrice(price: number, memeId: number, changedBy: string, dbForRW: db.Database): Promise<void> {
    return new Promise((resolve, reject) => {
        dbForRW.exec(`
        begin exclusive;
        insert or rollback into memes_price (id, price, meme_id, set_by) values (
            (select ifnull(max(id), 0) + 1 from memes_price), ${price}, ${memeId}, '${changedBy}');
        commit;
        `, (err: Error) => {
            if (err) {
                reject(err);
                return;
            }
            resolve();
        });
    });
}

export function getAllMemes(dbForRO: db.Database): Promise<MemeCurrentDao[]> {
    return new Promise((resolve, reject) => {
        dbForRO.all(`
        select id, name, url, ifnull(price, 0) as currentPrice from meme
        left join (
            select max(id), meme_id, price
            from memes_price
            group by meme_id)
        on id = meme_id;
        `, (err: Error, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data);
        });
    });
}

export function getMostValueableMemes(dbForRO: db.Database, limit: number = 3): Promise<MemeCurrentDao[]> {
    return new Promise((resolve, reject) => {
        dbForRO.all(`
        select id, name, url, ifnull(price, 0) as currentPrice from meme
        left join (
            select max(id), meme_id, price
            from memes_price
            group by meme_id)
        on id = meme_id
        order by currentPrice desc
        limit ?;
        `, [limit], (err: Error, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data);
        });
    });
}

export function getMemeById(id: number, dbForRO: db.Database): Promise<MemeWithHistoryPriceDao> {
    return new Promise((resolve, reject) => {
        dbForRO.all(`
        select meme.id as id, meme.name as name, meme.url as url, ifnull(memes_price.price, 0) as price
        from (select * from meme where id = ?) as meme
        left join memes_price on meme.id = memes_price.meme_id
        order by memes_price.id desc;
        `, [id], (err: Error, data) => {
            if (err) {
                reject(err);
                return;
            }
            if (data.length === 0) {
                reject(Error('Meme not exists'));
                return;
            }
            const meme: MemeWithHistoryPriceDao = {
                id: data[0].id,
                name: data[0].name,
                url: data[0].url,
                prices: [],
            }
            data.forEach(memePrice => {
                meme.prices.push(memePrice.price);
            });
            resolve(meme);
        });
    });
}