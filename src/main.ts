function wait(timeMillis: number): Promise<void> {
    return new Promise((resolve, _) => {
        window.setTimeout(resolve, timeMillis)
    });
}

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.loading-container').forEach((div: HTMLDivElement) => {
        wait(500).then(() => {
            div.classList.add('hidden');
            wait(500).then(() => {
                div.classList.add('removed');
            });
        });
    })
 }, false);