export function isValidMemePojoJson(jsonData: any): boolean {
    if ('object' !== typeof jsonData) {
        return false;
    }

    let hasId = false;
    let hasName = false;
    let hasUrl = false;
    let hasPrices = false;

    for (const property in jsonData) {
        if (property === 'id') {
            if (!isIntegerNumber(jsonData[property])) {
                return false;
            }
            hasId = true;
        }
        else if (property === 'name') {
            if (!isValidString(jsonData[property])) {
                return false;
            }
            hasName = true;
        }
        else if (property === 'url') {
            if (!isValidString(jsonData[property])) {
                return false;
            }
            hasUrl = true;
        }
        else if (property === 'prices') {
            if (!Array.isArray(jsonData[property])) {
                return false;
            }
            if (jsonData[property].length <= 0) {
                return false;
            }
            for (const price of jsonData[property]) {
                if (!isIntegerNumber(price)) {
                    return false;
                }
            }
            hasPrices = true;
        }
    }

    return hasId && hasName && hasUrl && hasPrices;
}

export interface MemePojo {
    id: number;
    name: string;
    url: string;
    prices: number[];
}

export class Meme {
    #id: number;
    #name: string;
    #url: string;
    #prices: number[];

    constructor(id: number, name: string, prices: number[], url: string) {
        this.#id = id;
        this.#name = name;
        this.#prices = prices;
        this.#url = url;
    }

    get id(): number {
        return this.#id;
    }

    get name(): string {
        return this.#name;
    }

    get prices(): number[] {
        return this.#prices.slice();
    }

    get currentPrice(): number {
        const id = this.#prices.length - 1;
        return this.#prices[id];
    }

    get url(): string {
        return this.#url;
    }

    addPrice(newPrice: number): void {
        this.#prices.push(newPrice);
    }

    toPojo(): MemePojo {
        return {
            id: this.#id,
            name: this.#name,
            prices: this.#prices,
            url: this.#url
        };
    }
}

function isValidString(data: any): boolean {
    return ('string' === typeof data && data.length > 0);
}

function isIntegerNumber(data: any): boolean {
    return ('number' === typeof data && Number.isInteger(data));
}
