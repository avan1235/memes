import { Meme } from './memes';

function compareMemes(first: Meme, second: Meme): number {
    return second.currentPrice - first.currentPrice;
}

export class MemesList {
    #collectedMemes: Map<number, Meme>;

    constructor(list: Meme[] = []) {
        this.#collectedMemes = new Map<number, Meme>();
        for (const meme of list) {
            this.#collectedMemes.set(meme.id, meme);
        }
    }

    addMeme(meme: Meme): void {
        this.#collectedMemes.set(meme.id, meme);
    }

    get mostValueable(): Meme[] {
        return Array.from(this.#collectedMemes.values()).sort(compareMemes).slice(0, 3);
    }

    get allCollected(): Meme[] {
        return Array.from(this.#collectedMemes.values()).slice();
    }

    byId(id: number): Meme {
        return this.#collectedMemes.get(id);
    }
}