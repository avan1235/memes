import { MemePojo, isValidMemePojoJson } from './memes.js';

let memeId: number;
let csrfToken: string;

initialFetch();

function initialFetch(): void {
    const maybeMemeId = /^\?id=(\d+)$/.exec(window.location.search);
    if (!maybeMemeId) {
        redirect('/error');
        return;
    }
    memeId = parseInt(maybeMemeId[1], 10);
    fetchAndUpdate();
}

function fetchAndUpdate(): void {
    fetch(`/meme/${memeId}`)
        .then((response) => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            csrfToken = response.headers.get('CSRF-Header');
            return response.json();
        }).then((memeJson) => {
            if (!isValidMemePojoJson(memeJson)) {
                throw Error('Invalid received data format');
            }
            const meme: MemePojo = memeJson;
            updatePrices(meme);
        }).catch((error) => {
            redirect('/error');
        });
}

function updatePrices(meme: MemePojo) {
    getDiv('name').textContent = `#${meme.id} ${meme.name}`;

    const prices =  getDiv('prices');
    removeChildrenOf(prices);

    const header = createDiv('th');
    header.textContent = 'Cena [zł]'
    const headerRow = createDiv('tr');
    headerRow.appendChild(header);
    prices.append(headerRow);
    for (const price of meme.prices) {
        const data = createDiv('td');
        data.textContent = `${price}`
        const dataRow = createDiv('tr');
        dataRow.appendChild(data);
        prices.append(dataRow);
    }
}

getDiv('price-form').addEventListener('submit', async (event) => {
    event.preventDefault();
    const priceValue = parseInt((document.querySelector('[name="price"]') as HTMLInputElement).value, 10);

    fetch(`/meme/${memeId}`, {
        method: 'POST',
        headers: {
            'Content-type': 'application/json',
            'CSRF-Token': csrfToken
        },
        body: JSON.stringify({ price: priceValue })})
        .then((response) => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            fetchAndUpdate()
        }).catch((error) => {
            redirect('/error');
        });
});

function getDiv(name: string): HTMLDivElement {
    return document.getElementById(name) as HTMLDivElement;
}

function createDiv(type: string = 'div'): HTMLDivElement {
    return document.createElement(type) as HTMLDivElement;
}

function removeChildrenOf(el: HTMLDivElement): void {
    while (el.firstChild) {
        el.removeChild(el.lastChild);
    }
}

function redirect(location: string): void {
    window.location.replace(location);
}