import app from './app';
import { echo } from 'shelljs';

// Run server instance
const port = app.get('port');
const server = app.listen(port, () => {
    echo(`App listening at http://localhost:${port}`);
});

export default server;
