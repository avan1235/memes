import 'mocha';
import { expect } from 'chai';
import { Meme } from '../src/memes';
import { MemesList } from '../src/memes_list';

describe('MemesList tested', () => {

    it('should give empty lists when no data added', () => {
        expect(new MemesList().allCollected.length).to.equal(0);
        expect(new MemesList().mostValueable.length).to.equal(0);
    });

    it('should give empty lists when empty data added', () => {
        expect(new MemesList([]).allCollected.length).to.equal(0);
        expect(new MemesList([]).mostValueable.length).to.equal(0);
    });

    it('should give best memes with highest prices', () => {
        const memes = memesListWithPrices([100, 600, 300, 500, 200]);
        memesListCheckPrices(memes, [300, 500, 600]);
    });

    it('should give best 3 memes if enough given', () => {
        const memes = memesListWithPrices([100, 600, 300, 500, 200]);
        expect(memes.mostValueable.length).to.equal(3);
    });

    it('should give best 2 memes if no more given', () => {
        const memes = memesListWithPrices([100, 600]);
        expect(memes.mostValueable.length).to.equal(2);
        memesListCheckPrices(memes, [100, 600]);
    });

    it('should give best 1 meme if no more given', () => {
        const memes = memesListWithPrices([500]);
        expect(memes.mostValueable.length).to.equal(1);
        memesCheckMostValueableContains(memes, 500);
    });

    it('should take into account only current prices', () => {
        const memes = memesListWithPrices([300, 200, 100, 600, 500, 400]);
        memesListCheckPrices(memes, [400, 500, 600]);
        [1, 2, 3, 4, 5, 6].forEach((price, index) => {
            memes.byId(index).addPrice(price);
        });
        memesListCheckPrices(memes, [4, 5, 6]);
        [1000, 1200, 1500, 1300, 1100, 1400].forEach((price, index) => {
            memes.byId(index).addPrice(price);
        });
        memesListCheckPrices(memes, [1500, 1300, 1400]);
    });
});

function memesListWithPrices(prices: number[]): MemesList {
    const list = new MemesList();
    prices.forEach((price, index) => {
        list.addMeme(new Meme(index, `FakedMeme${index}`, price, 'nodata'));
    });
    return list;
}

function memesCheckMostValueableContains(memes: MemesList, price: number): void {
    expect(memes.mostValueable.map(meme => meme.currentPrice)).to.contain(price);
}

function memesListCheckPrices(memes: MemesList, prices: number[]): void {
    prices.forEach(price => {
        memesCheckMostValueableContains(memes, price);
    });
}